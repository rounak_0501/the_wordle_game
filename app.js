let row=1,col=1,enter=1,lastenter=1;
const words = ["ABOUT","ABOVE","AFTER","AGAIN","ALONE","APPLE","BEACH","BEGIN","BLACK","BRING","BROWN","BUNNY","CAMEL","CANDY","CARRY","CHILD","CLEAN","CLOSE","COUNT","DADDY","DREAM","DRESS","DRIVE","EIGHT","EVERY","FIGHT","FLOOR","FOUND","GHOST","GOOSE","GREAT","GREEN","HAPPY","HEARD","HEART","HIPPO","HORSE","HOUSE","INDIA","JUICE","KOALA","LARGE","LIGHT","LUCKY","MOMMY","MONEY","MOOSE","MOUSE","MUMMY","MUSIC","NEVER","NURSE","PANDA","PAPER","PARTY","PIZZA", "PLANE", "PLANT", "PLATE", "PRICE", "PUPPY", "QUACK","QUEEN", "QUIET", "RIGHT", "RIVER", "ROBIN", "ROBOT", "ROUND","SEVEN", "SHEEP", "SKUNK", "SLEEP", "SMALL", "SPOON", "STAMP","STAND", "STICK", "STORE", "STORY", "STRAY", "SUNNY", "SWEET","TABLE", "THERE", "THING", "THREE", "TIGER", "TODAY", "TRAIN","TRUCK", "TUMMY", "UNDER", "WATER", "WHITE", "WITCH", "WOMAN","WOMEN", "WRITE", "ZEBRA"];
const hidden = words[Math.floor(Math.random()*100+1)%words.length];
console.log(hidden);
function change(letter){
    if(col>=6){
        alert("Please press Enter!!!");
        return;
    }
    document.querySelector(".r"+row).querySelector(".c"+col).innerHTML=letter;
    col++;
}

function submit(){
    if(col<6){
        alert("Word not complete!");
        return;
    }

    // colour changing logic
    const notFound=[];
    for(let column=1;column<6;column++){
        let node=document.querySelector(".r"+row).querySelector(".c"+column);
        if(node.innerHTML===hidden[column-1])
            node.style.background="green";
        else
            notFound.push(column);
    }
    if(notFound.length===0){
        alert("Hurray!!!! You won the game");
        return;
    }
    let matched=notFound.map(x=>0);
    for(let i=0;i<notFound.length;i++){
        let r=notFound[i],c;
        if(matched[r])continue;
        for(let j=0;j<notFound.length;j++){
            c=notFound[j];
            let node=document.querySelector(".r"+row).querySelector(".c"+c);
            if(node.innerHTML===hidden[r]){
                matched[r]=1;
                node.style.background="yellow";
                break;
            }
        }
    }

    lastenter=enter;
    enter++;
    if(row===5){
        alert("Game Complete");
        return;
    }
    row++;
    col=1;
}

function clearBlock(){
    if(col>1){
        col--;
    }
    document.querySelector(".r"+row).querySelector(".c"+col).innerHTML='';
}